### 2018 IML challenge 

The challenge detail are decribed in the wiki. To submit a solution (no later than 10th of April 24:00 CERN time) please make a PR request of a new file in the solutions directory. The commit comment should include your name and the file should be either numpy or root. 